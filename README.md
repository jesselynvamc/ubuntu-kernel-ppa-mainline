## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-ubuntu-kernel-ppa-mainline.asc https://packaging.gitlab.io/ubuntu-kernel-ppa-mainline/gpg.key
```

## Add repo to apt

```bash
apt-get install apt-transport-https
echo "deb [arch=amd64] https://packaging.gitlab.io/ubuntu-kernel-ppa-mainline ubuntu-kernel-mainline main" | sudo tee /etc/apt/sources.list.d/morph027-ubuntu-kernel-ppa-mainline.list
```

## Installation

```bash
# latest
sudo apt-get install linux-mainline
# specific version
sudo apt-get install linux-mainline-5.9
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
