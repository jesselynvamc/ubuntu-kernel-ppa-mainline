#!/bin/bash

set -euo pipefail

description="Mainline Generic Linux kernel and headers
This package will always depend on the latest mainline generic Linux kernel
and headers."
version="${CI_COMMIT_TAG%-*}"
if [[ "${CI_COMMIT_TAG}" =~ ^.*-.*$ ]]; then
    patchlevel="${CI_COMMIT_TAG#*-}"
else
    patchlevel="1"
fi

apt-get -qqy update
apt-get -qqy install \
    curl \
    ruby \
    ruby-dev \
    ruby-ffi \
    make \
    gcc \
    git \
    libdigest-sha-perl

curl -sLO "https://kernel.ubuntu.com/~kernel-ppa/mainline/v${version}/amd64/CHECKSUMS"
sed -i '/lowlatency/d' CHECKSUMS
mapfile -t packages < <(grep -Eo 'linux-.*.deb' CHECKSUMS | sort -u)
dependencies=()
for package in "${packages[@]}"
do
    curl -sLO "https://kernel.ubuntu.com/~kernel-ppa/mainline/v${version}/amd64/${package}"
    dependencies+=( "--depends $(dpkg -I "${package}" | grep 'Package:' | cut -d' ' -f3)" )
done
shasum -c CHECKSUMS
gem install fpm

# meta package including version
fpm \
    --input-type empty \
    --output-type deb \
    --name "linux-mainline-${version%.*}" \
    --version "${version}-${patchlevel}" \
    --description "${description}" \
    --maintainer "Ubuntu Kernel Team https://wiki.ubuntu.com/KernelTeam" \
    --url "https://kernel.ubuntu.com/~kernel-ppa/mainline/v${version}" \
    --deb-recommends morph027-keyring \
    ${dependencies[*]}

# meta package
fpm \
    --input-type empty \
    --output-type deb \
    --name "linux-mainline" \
    --version "${version}-${patchlevel}" \
    --description "${description}" \
    --maintainer "Ubuntu Kernel Team https://wiki.ubuntu.com/KernelTeam" \
    --url "https://kernel.ubuntu.com/~kernel-ppa/mainline/v${version}" \
    --depends "linux-mainline-${version%.*}"
